TARGET=$@
PYTHON_VERSION=$(lastword $(subst -, ,$(TARGET)))

all: docker-latest

tests:
	python3 setup.py test

# Default rule to handle any python version
tests-docker-%:

	echo "Running tests for python $(PYTHON_VERSION)"

	docker build . \
		-f tests/Dockerfile \
		-t woger_tests:$(PYTHON_VERSION) \
		--build-arg PYTHON_VERSION=$(PYTHON_VERSION)

	docker run woger_tests:$(PYTHON_VERSION)


docker-latest: tests-docker-3.6

docker-all: tests-docker-3.4 tests-docker-3.5 tests-docker-3.6

pypi:
	docker build . \
		-t woger_pypi:latest
	docker run \
		-e "PYPI_USERNAME=$(PYPI_USERNAME)" \
		-e "PYPI_PASSWORD=$(PYPI_PASSWORD)" \
		woger_pypi:latest

.PHONY: tests all

