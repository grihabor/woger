import pytest


def test_data_invalid_attribute():
    from woger import BaseData

    data = BaseData()

    with pytest.raises(AttributeError):
        print(data.catalog)
