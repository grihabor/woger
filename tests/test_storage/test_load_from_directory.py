import pytest


@pytest.mark.parametrize('ws_ids', [
    ([]),
    ([9]),
    ([9, 17]),
    ([134, 9, 17]),
    ([134, 235, 9, 17]),
])
def test_storage_load_from_directory(ws_ids, tmpdir):
    from woger import WorkspaceStorage, Workspace, WorkspaceManager

    class OrderedWorkspace(Workspace):
        def __lt__(self, other):
            return self.id < other.id

    root = tmpdir
    for name in ws_ids:
        wm = WorkspaceManager(root)
        wm.create(name)

    storage = WorkspaceStorage.load_from_directory(
        root,
        workspace_cls=OrderedWorkspace,
    )

    assert len(storage.workspaces) == len(ws_ids)
    assert set(ws for ws in storage) == set(ws_ids)
