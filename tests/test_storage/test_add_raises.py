import pytest


@pytest.mark.parametrize('value', [134, 'adfgv', object()])
def test_storage_add_raises(value):
    from woger import WorkspaceStorage

    wst = WorkspaceStorage()

    with pytest.raises(ValueError):
        wst.add(value)
