import itertools
import random
from collections import OrderedDict
import pytest


def test_storage_iter_empty():
    from woger import WorkspaceStorage

    storage = WorkspaceStorage()

    assert [] == [path for path in storage]


_test_keys = [
    [],
    ['146', '1345'],
    ['112', '265', '472', '784'],
    ['a', 'b', 'c', 'd'],
]

_test_methods = [
    'items',
    'values',
    'keys',
]


def shuffle(arr):
    arr = list(arr)
    random.shuffle(arr)
    return arr


_preprocess = [
    shuffle,
    reversed,
]

_test_data = itertools.product(_test_keys, _test_methods, _preprocess)


@pytest.mark.parametrize('keys,method_name,preprocess', _test_data)
def test_storage_fill(keys, method_name, preprocess):
    from woger import WorkspaceStorage, Workspace, BasePathStructure

    storage = WorkspaceStorage()
    ws_mapping = OrderedDict()

    for key in keys:
        ws = Workspace.construct(key, BasePathStructure)
        ws_mapping[ws.id] = ws

    for key in preprocess(keys):
        ws = Workspace.construct(key, BasePathStructure)
        storage.add(ws)

    mapping_method = getattr(ws_mapping, method_name)
    storage_method = getattr(storage, method_name)

    for real_values, values in zip(mapping_method(), storage_method()):
        assert real_values == values


