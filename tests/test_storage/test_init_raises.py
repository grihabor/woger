import pytest


@pytest.mark.parametrize('value', [134, 'asgfq', object()])
def test_storage_init_raises(value):
    from woger import WorkspaceStorage

    with pytest.raises(ValueError):
        WorkspaceStorage(value)
