import os
import pytest


@pytest.mark.parametrize('subdirs,expected', [
    (['134', '235', '9', '17'], 235),
    (['134', '235', '579', '17'], 579),
    (['134', '35', '9', '17'], 134),
])
def test_manager_latest_workspace(subdirs, expected, tmpdir):
    from woger import WorkspaceManager, Workspace, BasePathStructure

    class OrderedWorkspace(Workspace):
        def __lt__(self, other):
            return self.id < other.id

    root = tmpdir

    for path in subdirs:
        WorkspaceManager(root).create(path)

    wm = WorkspaceManager(root, workspace_cls=OrderedWorkspace)

    assert wm.latest().id == expected
