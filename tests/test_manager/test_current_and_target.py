from functools import partial


def _get_wm(root, WorkspaceManager):
    return WorkspaceManager(root)


def test_manager_current_target_update(tmpdir):
    from woger import WorkspaceManager

    root = tmpdir
    _wm = partial(_get_wm, root, WorkspaceManager)
    _wm().create(126)

    assert _wm().current() is None
    assert _wm().target() is None

    _wm().target_latest()

    assert _wm().current() is None
    assert _wm().target().id == 126

    _wm().create(427)

    assert _wm().current() is None
    assert _wm().target().id == 126

    _wm().update()

    assert _wm().current().id == 126
    assert _wm().target().id == 126
