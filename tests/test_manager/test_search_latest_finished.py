import logging
from itertools import starmap
from pathlib import Path

import os
from random import randint
import random
from typing import Iterable

import pytest


def _get_logger():
    return logging.getLogger(__name__)


def _finish_action(ws, path):
    with ws.track(path.action()):
        with open(path, 'w'):
            pass


def _create_ws(wm, ws_id, finished):
    ws = wm.create(ws_id)
    if finished[0]:
        _finish_action(ws, ws.path.raw_xml)
    if finished[1]:
        _finish_action(ws, ws.path.parsed)


def _setup(workspaces,
           root,
           PathStructure,
           WorkspaceManager):
    wm = WorkspaceManager(root, PathStructure)

    for ws_id, finished in workspaces.items():
        _create_ws(wm, ws_id, finished)


@pytest.mark.parametrize('workspaces,expected', [
    ({
         126: [True, False],
         395: [True, False],
         832: [True, False],
     }, None),
    ({
         126: [True, True],
         395: [True, False],
         832: [True, False],
     }, 126),
    ({
        126: [True, True],
        395: [True, True],
        832: [True, False],
    }, 395),
    ({
        126: [True, True],
        395: [True, True],
        832: [True, True],
    }, 832),
])
def test_manager_search_latest_finished(workspaces, expected, tmpdir):
    from woger import WorkspaceManager, BasePathStructure

    class PathStructure(BasePathStructure):
        raw_xml = 'raw.xml'
        parsed = 'parsed.json'

    root = tmpdir

    args = [root, PathStructure, WorkspaceManager]
    _setup(workspaces, *args)

    wm = WorkspaceManager(root, PathStructure)
    ws = wm.find_latest_finished(PathStructure.parsed.action())

    if expected is not None:
        assert expected == ws.id
    else:
        assert ws is None
