import json
from pathlib import Path

import pytest


def set_value(key, value, path, WorkspaceState):
    ws = WorkspaceState(path)
    ws[key] = value


def check(key, value, path, WorkspaceState):
    ws = WorkspaceState(path)
    assert ws[key] == value


def get_set(path, WorkspaceState):
    return {
        item
        for item
        in WorkspaceState(path)
    }


def test_state_consistency(tmpdir):
    from woger import WorkspaceState

    path = tmpdir.join('.state.json')
    args = path, WorkspaceState

    set_value('a', 'apple', *args)
    check('a', 'apple', *args)
    check('b', None, *args)

    set_value('b', 'banana', *args)
    check('a', 'apple', *args)
    check('b', 'banana', *args)


def test_state_iteration(tmpdir):
    from woger import WorkspaceState

    path = tmpdir.join('.state.json')
    args = path, WorkspaceState

    assert set() == get_set(*args)

    set_value('a', 'apple', *args)
    assert {'a'} == get_set(*args)

    set_value('b', 'banana', *args)
    assert {'a', 'b'} == get_set(*args)
