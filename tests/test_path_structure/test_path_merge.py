import os


def test_path_structure_merge(tmpdir):
    from woger import BasePathStructure

    class A(BasePathStructure):
        xml = 'xml'

    class B(BasePathStructure):
        json = 'json'

    class C(A, B):
        txt = 'txt'

    root = str(tmpdir)
    ps = C(root)

    assert ps.json == os.path.join(root, 'json')
    assert ps.xml == os.path.join(root, 'xml')
    assert ps.txt == os.path.join(root, 'txt')
