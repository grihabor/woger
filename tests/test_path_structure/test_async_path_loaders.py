import json
from threading import Thread

import time


def test_async_path_loaders(tmpdir):
    from woger import BasePathStructure, Bind

    def target(path, root, action):
        ps = PathStructure(root)
        with ps.track(action):
            with open(path, 'w') as f:
                json.dump(dict(test='async'), f)

    def loader(*args):
        t = Thread(target=target, args=args)
        t.start()

    class PathStructure(BasePathStructure):
        loaded = Bind('loaded.json', loader)

    root = tmpdir
    ps = PathStructure(root)

    while True:
        if ps.loaded:
            break
        time.sleep(0.01)

    with open(ps.loaded, 'r') as f:
        assert json.load(f) == dict(test='async')


