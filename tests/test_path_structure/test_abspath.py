
def test_path_structure_abspath(tmpdir):
    from woger.base_path_structure import BasePathStructure

    class PathStructure(BasePathStructure):
        images = 'image_storage'
        xml = 'xml_data'
        css = 'css_files'
        js = 'js_files'

    root = tmpdir
    ps = PathStructure(root)

    assert ps.images == root.join('image_storage')
    assert ps.css == root.join('css_files')
    assert ps.js == root.join('js_files')
    assert ps.xml == root.join('xml_data')
