
def test_path_structure_attributes(tmpdir):
    from woger import BasePathStructure

    class PathStructure(BasePathStructure):
        checkpoints = 'checkpoints'
        summary = 'summary'

    ps = PathStructure(tmpdir)

    print(ps.summary)
    assert hasattr(ps, 'summary')
    assert hasattr(ps, 'checkpoints')
