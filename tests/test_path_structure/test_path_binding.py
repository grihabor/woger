import pytest


@pytest.mark.parametrize('fail', [True, False])
def test_binding(fail, tmpdir):
    from woger import BasePathStructure, Bind

    content = 'Example of binding'

    def create_example_txt(path: str, ws_root: str, action: str):
        try:
            with BasePathStructure(ws_root).track(action):
                with open(path, 'w') as f:
                    f.write(content)

                if fail:
                    raise RuntimeError('Something went wrong...')
        except RuntimeError:
            pass

    class PathStructure(BasePathStructure):
        example = Bind('example.txt', create_example_txt)

    ps = PathStructure(tmpdir)

    if fail:
        assert ps.example is None
    else:
        with open(ps.example, 'r') as f:
            assert f.read() == content
