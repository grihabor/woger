import pytest


class _ParseException(Exception):
    pass


def _body(ws):

    load_tracker = ws.track(ws.path.raw_xml.action())
    with load_tracker:
        assert load_tracker.started()
        assert not load_tracker.finished()
        assert not load_tracker.failed()

    assert load_tracker.finished()
    assert not load_tracker.started()
    assert not load_tracker.failed()

    parse_tracker = ws.track(ws.path.parsed.action())
    try:
        with parse_tracker:
            assert parse_tracker.started()
            assert not parse_tracker.failed()
            assert not parse_tracker.finished()
            assert load_tracker.finished()
            raise _ParseException()
    except _ParseException:
        pass

    assert parse_tracker.failed()
    assert not parse_tracker.finished()
    assert not parse_tracker.started()
    assert load_tracker.finished()
    assert not load_tracker.started()
    assert not load_tracker.failed()


def test_tracker_action_state(tmpdir):
    from woger import Workspace, BasePathStructure

    class PathStructure(BasePathStructure):
        raw_xml = 'raw.xml'
        parsed = 'parsed.json'

    ws = Workspace.construct(str(tmpdir), PathStructure)
    _body(ws)
