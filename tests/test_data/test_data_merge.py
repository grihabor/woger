

def test_data_merge():
    from woger import BaseData

    class A(BaseData):
        xml = lambda data: 'xml'

    class B(BaseData):
        json = lambda data: 'convert {} to json'.format(data.xml)

    class C(A, B):
        all = lambda data: (data.xml, data.json)

    data = C()
    assert data.all == ('xml', 'convert xml to json')
