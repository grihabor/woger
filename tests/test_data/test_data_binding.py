
def test_data_binding(tmpdir):
    from woger import Workspace, BasePathStructure, BaseData

    class PathStructure(BasePathStructure):
        json = 'json'
        xml = 'xml'

    def load_json(data):
        return '{}'

    def convert_to_xml(data):
        return (
            data.json
            .replace('{', '<data>')
            .replace('}', '</data>')
        )

    class Data(BaseData):
        json = load_json
        xml = convert_to_xml

    ws = Workspace.construct(str(tmpdir), PathStructure, Data)
    assert ws.data.json == '{}'
    assert ws.data.xml == '<data></data>'
