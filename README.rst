=====
Woger
=====

|pipeline-status|
|test-coverage|
|docs-status|
|pypi-version|
|pypi-python-versions|
|license|

Workspace manager library

Why woger?
==========

Woger allows you to

- easily create directory structure
- manage multiple versions of your directories
- track the state of each workspace
- transition from one version to another

Documentation
=============

Woger documentation at readthedocs: http://woger.rtfd.io

.. |pipeline-status| image:: https://gitlab.com/grihabor/woger/badges/master/pipeline.svg
   :target: https://gitlab.com/grihabor/woger/commits/master

.. |test-coverage| image:: https://gitlab.com/grihabor/woger/badges/master/coverage.svg
   :target: https://gitlab.com/grihabor/woger/commits/master

.. |pypi-version| image:: https://img.shields.io/pypi/v/woger.svg 
   :target: https://pypi.python.org/pypi/woger

.. |license| image:: https://img.shields.io/pypi/l/woger.svg 
   :target: https://pypi.python.org/pypi/woger

.. |pypi-python-versions| image:: https://img.shields.io/pypi/pyversions/woger.svg
   :target: https://pypi.python.org/pypi/woger

.. |docs-status| image:: https://readthedocs.org/projects/woger/badge/?version=latest
   :target: https://woger.readthedocs.io/en/latest/?badge=latest 
