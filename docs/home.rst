==========
Woger home
==========

Woger — Python library for workspace management

Why Woger?
==========

Woger allows you to

- easily create directory structure
- manage multiple versions of your directories
- track the state of each workspace
- transition from one version to another

Installation
============

.. code-block:: bash

    pip3 install woger

Examples
========

Path
----

.. toctree::

    examples/path/basic_path_structure
    examples/path/path_loader_binding
    examples/path/composition_of_path_structures

Data
----

.. toctree::

    examples/data/basic_data_loader
    examples/data/pass_data_loader_args
    examples/data/chained_data_loaders
    examples/data/composition_of_data

Storage
-------

.. toctree::

    examples/storage/basic_storage

Workspace
---------

.. toctree::

    examples/workspace/basic_workspace
    examples/workspace/workspace_with_chained_loaders

Workspace manager
-----------------

.. toctree::

    examples/manager/workspace_management
    examples/manager/manager_with_data_bindings

License
=======

MIT
