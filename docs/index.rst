.. woger documentation master file, created by
   sphinx-quickstart on Wed Dec  6 09:09:18 2017.

=================================
Welcome to Woger's documentation!
=================================

.. toctree::
   :maxdepth: 2

   home
   api
   api/woger
