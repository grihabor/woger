===
API
===

.. autosummary::

    woger.ActionStatus
    woger.ActionTracker
    woger.WorkspaceManager
    woger.WorkspaceState
    woger.WorkspaceStorage
    woger.Workspace
    woger.BasePathStructure
    woger.BaseData


