.. _workspace-with-chained-loaders:

Workspace with chained loaders
------------------------------

.. code-block:: python

    >>> from woger import Workspace, BasePathStructure, BaseData
    >>>
    >>> class PathStructure(BasePathStructure):
    ...     json = 'json'
    ...     xml = 'xml'
    >>>
    >>> def load_json(data):
    ...     return '{}'
    >>>
    >>> def convert_to_xml(data):
    ...     return (
    ...         data.json
    ...         .replace('{', '<data>')
    ...         .replace('}', '</data>')
    ...     )
    >>>
    >>> class Data(BaseData):
    ...     json = load_json
    ...     xml = convert_to_xml
    >>>
    >>> ws = Workspace.construct('root', PathStructure, Data)
    >>> print(ws.data.xml)
    <data></data>



