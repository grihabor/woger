.. _basic-workspace:

Basic workspace
---------------

.. code-block:: python

    >>> from woger import Workspace, BasePathStructure, BaseData
    >>>
    >>> class PathStructure(BasePathStructure):
    ...     json = 'json'
    ...     xml = 'xml'
    >>>
    >>> def load_json(ws: Workspace):
    ...     return '{}'
    >>>
    >>> class Data(BaseData):
    ...     json = load_json
    >>>
    >>> ws = Workspace.construct('root', PathStructure, Data)
    >>> print(ws.data.json)
    {}



