.. _basic-data-loader:

Basic data loader
-----------------

.. code-block:: python

    >>> from woger import Workspace, BasePathStructure, BaseData
    >>>
    >>> def load_json(ws):
    ...     return '{}'
    >>>
    >>> class Data(BaseData):
    ...     json = load_json
    >>>
    >>> data = Data()
    >>> print(data.json)
    {}

