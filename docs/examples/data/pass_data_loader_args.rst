.. _pass-data-loader-args:

Pass data loader args
---------------------

.. code-block:: python

    >>> import json
    >>> from woger import Workspace, BasePathStructure, BaseData
    >>>
    >>> def load_json(data):
    ...     return json.dumps(data.passed_arg)
    >>>
    >>> class Data(BaseData):
    ...     passed_arg = lambda data: dict(value=[4, 2])
    ...     json = load_json
    >>>
    >>> data = Data()
    >>> print(data.json)
    {"value": [4, 2]}
