.. _chained-data-loaders:

Chained data loaders
--------------------

.. code-block:: python

    >>> from woger import BaseData
    >>>
    >>> def load_json(data):
    ...     return '{}'
    >>>
    >>> def convert_to_xml(data):
    ...     return (
    ...         data.json
    ...         .replace('{', '<data>')
    ...         .replace('}', '</data>')
    ...     )
    >>>
    >>> class Data(BaseData):
    ...     json = load_json
    ...     xml = convert_to_xml
    >>>
    >>> data = Data()
    >>> print(data.xml)
    <data></data>



