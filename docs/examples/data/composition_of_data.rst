.. _composition-of-data:

Composition of data
-------------------

.. code-block:: python

    >>> from woger import BaseData
    >>>
    >>> class A(BaseData):
    ...     xml = lambda data: 'xml'
    >>>
    >>> class B(BaseData):
    ...     json = lambda data: 'convert {} to json'.format(data.xml)
    >>>
    >>> class C(A, B):
    ...     all = lambda data: (data.xml, data.json)
    >>>
    >>> data = C()
    >>> print(data.xml, data.json, data.all, sep='\n')
    xml
    convert xml to json
    ('xml', 'convert xml to json')
