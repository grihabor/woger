.. _basic-path-structure:

Basic path structure
--------------------

.. code-block:: python

    >>> from woger import Workspace, BasePathStructure
    >>>
    >>> class PathStructure(BasePathStructure):
    ...     images = 'img'
    ...     json = 'json'
    ...     xml = 'xml'
    >>>
    >>> path = Workspace.construct('root', PathStructure).path
    >>> print(path.images)
    root/img

    
