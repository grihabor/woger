.. _path-loader-binding:

Path loader binding
-------------------

.. code-block:: python

    >>> from woger import BasePathStructure, Bind, Workspace
    >>> import tempfile
    >>>
    >>> content = 'Example of binding'
    >>>
    >>> def create_example_txt(path, ws_root, action):
    ...     ws = Workspace.construct(ws_root)
    ...     with ws.track(action):
    ...         print('Creating example.txt...')
    ...         with open(str(path), 'w') as f:
    ...             f.write(content)
    ...     return True
    ...
    >>>
    >>> class PathStructure(BasePathStructure):
    ...     example = Bind('example.txt', create_example_txt)
    ...
    >>>
    >>> with tempfile.TemporaryDirectory() as root:
    ...     ps = Workspace.construct(root, PathStructure).path
    ...
    ...     with open(str(ps.example), 'r') as f:
    ...         print(f.read())
    ...
    Creating example.txt...
    Example of binding
