.. _composition-of-path-structures:

Composition of path structures
------------------------------

.. code-block:: python

    >>> from woger import BasePathStructure
    >>>
    >>> class PS1(BasePathStructure):
    ...     xml = 'xml'
    >>>
    >>> class PS2(BasePathStructure):
    ...     json = 'json'
    >>>
    >>> class PathStructure(PS1, PS2):
    ...     txt = 'txt'
    >>>
    >>> ps = PathStructure('root')
    >>> print(ps.txt, ps.json, ps.xml, sep='\n')
    root/txt
    root/json
    root/xml
