.. _workspace-management:

Workspace management
--------------------

.. code-block:: python

    >>> from woger import WorkspaceManager, BasePathStructure
    >>> import tempfile
    >>>
    >>> class PathStructure(BasePathStructure):
    ...     loaded = 'loaded.xml'
    ...     parsed = 'parsed.json'
    >>>
    >>> manager = WorkspaceManager('store', PathStructure)
    >>>
    >>> manager.create(1346)
    <Workspace(store/1346)>
    >>>
    >>> manager.create(542)
    <Workspace(store/542)>
    >>>
    >>> manager.target_latest()
    >>> manager.target()
    <Workspace(store/1346)>
    >>>
    >>> manager.current() is None
    True
    >>>
    >>> manager.update()
    >>> manager.current() is manager.target()
    True
