.. _manager-with-data-bindings:

Manager with data bindings
--------------------------

.. code-block:: python

    >>> from woger import WorkspaceManager, BasePathStructure, BaseData
    >>> from collections import OrderedDict
    >>>
    >>> class PathStructure(BasePathStructure):
    ...     loaded = 'loaded.txt'
    ...     parsed = 'parsed.json'
    >>>
    >>> def load_txt(data):
    ...     return '\n'.join([
    ...         'This text-mapping of values',
    ...         'Goal-convert to json dict',
    ...     ])
    >>>
    >>> def parse_loaded(data):
    ...     content = data.loaded
    ...     return OrderedDict(
    ...         line.split('-')
    ...         for line
    ...         in content.split('\n')
    ...     )
    >>>
    >>> class Data(BaseData):
    ...     loaded = load_txt
    ...     parsed = parse_loaded
    >>>
    >>> manager = WorkspaceManager('storage', PathStructure, Data)
    >>>
    >>> ws = manager.create(1346)
    >>> print(ws)
    <Workspace(storage/1346)>
    >>>
    >>> print(manager)
    <WorkspaceManager(root=storage, storage=<WorkspaceStorage [1346]>)>
    >>>
    >>> ws.data.parsed
    OrderedDict([('This text', 'mapping of values'), ('Goal', 'convert to json dict')])
