.. _basic-storage:

Basic storage
-------------

.. code-block:: python

    >>> from woger import WorkspaceStorage, BasePathStructure, Workspace
    >>>
    >>> storage = WorkspaceStorage()
    >>>
    >>> print(len(storage))
    0
    >>>
    >>> ws = Workspace.construct('257', BasePathStructure)
    >>> storage.add(ws)
    >>>
    >>> print(len(storage))
    1
    >>> ws = Workspace.construct('175', BasePathStructure)
    >>> storage.add(ws)
    >>>
    >>> print(len(storage))
    2
    >>> print(storage)
    <WorkspaceStorage [175, 257]>

